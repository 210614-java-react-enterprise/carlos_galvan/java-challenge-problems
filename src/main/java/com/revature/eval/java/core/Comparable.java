package com.revature.eval.java.core;

public interface Comparable<T> {
    public int compareTo(T o);
}