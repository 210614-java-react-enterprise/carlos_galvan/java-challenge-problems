package com.revature.eval.java.core;

// CARLOS GALVAN 2021
// 17 / 20 question successfully answered

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.*;
//import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class EvaluationService {

    /**
     * DONE
     * 1. Without using the StringBuilder or StringBuffer class, write a method that
     * reverses a String. Example: reverse("example"); -> "elpmaxe"
     *
     * @param string
     * @return
     */
    public String reverse(String string) {

        char[] ch = string.toCharArray();

        for (int i = 0; i < ch.length / 2; i++) {
            char temp = ch[i];
            ch[i] = ch[ch.length - i - 1];
            ch[ch.length - i - 1] = temp;
        }
        string = String.valueOf(ch);
        return string;
    }

    /**
     * DONE
     * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
     * Acronyms)! Help generate some jargon by writing a program that converts a
     * long name like Portable Network Graphics to its acronym (PNG).
     *
     * @param phrase
     * @return
     */
    public String acronym(String phrase) {
        // TODO Write an implementation for this method declaration
        char[] ch = phrase.toCharArray(); //convert to array of chars
        if (ch.length <= 0) {// catch if its empty
            return null;
        }
        String temp = ""; //temp strings will contain final string
        temp += ch[0];//add first character
        for (int i = 0; i < ch.length; i++) { //loop through (index needed to get next element
            //if the character is a whitespace or a non alphabetical char add the next element(aslong if its not a whitespace)
            if ((Character.isWhitespace(ch[i]) || !Character.isLetterOrDigit(ch[i])) && !Character.isWhitespace(ch[i + 1])) {
                temp += ch[i + 1];//add char to string
            }
        }

        return temp.toUpperCase(); //convert to uppercase
    }

    /**
     * DONE
     * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
     * equilateral triangle has all three sides the same length. An isosceles
     * triangle has at least two sides the same length. (It is sometimes specified
     * as having exactly two sides the same length, but for the purposes of this
     * exercise we'll say at least two.) A scalene triangle has all sides of
     * different lengths.
     */
    static class Triangle {
        private double sideOne;
        private double sideTwo;
        private double sideThree;

        public Triangle() {
            super();
        }

        public Triangle(double sideOne, double sideTwo, double sideThree) {
            this();
            this.sideOne = sideOne;
            this.sideTwo = sideTwo;
            this.sideThree = sideThree;
        }

        public double getSideOne() {
            return sideOne;
        }

        public void setSideOne(double sideOne) {
            this.sideOne = sideOne;
        }

        public double getSideTwo() {
            return sideTwo;
        }

        public void setSideTwo(double sideTwo) {
            this.sideTwo = sideTwo;
        }

        public double getSideThree() {
            return sideThree;
        }

        public void setSideThree(double sideThree) {
            this.sideThree = sideThree;
        }

        //This method compares three sides with each others and generates a ratio of instances where matching cases to non matching cases
        public int[] getSideRatio() {
            double[] side = {getSideOne(), getSideTwo(), getSideThree()};
            int eq = 0;
            int noEq = 0;
            for (double si : side) {
                for (double de : side) {
                    if (si == de) {
                        eq++;
                    } else {
                        noEq++;
                    }
                }
            }
            int[] ratio = {eq, noEq};
            return ratio;

        }

        //look at getSideRatio
        //If
        public boolean isEquilateral() {
            // TODO Write an implementation for this method declaration
//            if ((getSideOne() == getSideTwo()) && getSideOne() == getSideThree()) {
//                return true;
//            }
            int[] sides = getSideRatio();

            if (sides[1] == 0) {
                return true;
            }
            return false;
        }

        public boolean isIsosceles() {
            // TODO Write an implementation for this method declaration
            int[] sides = getSideRatio();

            if (sides[0] > sides[1] && sides[1] > 0) {
                return true;
            }
            return false;
        }

        public boolean isScalene() {
            // TODO Write an implementation for this method declaration
            int[] sides = getSideRatio();

            if (sides[0] < sides[1] && sides[1] > 0) {
                return true;
            }
            return false;
        }

    }

    /**
     * DONE
     * 4. Given a word, compute the scrabble score for that word.
     * <p>
     * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
     * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
     * "cabbage" should be scored as worth 14 points:
     * <p>
     * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
     * point for E And to total:
     * <p>
     * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
     *
     * @param string
     * @return
     */
    public int getScrabbleScore(String string) {
        // TODO Write an implementation for this method declaration

        int total = 0;
        //Hashmap to store the score pattern
        HashMap<Character, Integer> scores = new HashMap<>();
        //One point A, E, I, O, U, L, N, R, S, T = 1;
        scores.put('A', 1);
        scores.put('E', 1);
        scores.put('I', 1);
        scores.put('O', 1);
        scores.put('U', 1);
        scores.put('L', 1);
        scores.put('N', 1);
        scores.put('R', 1);
        scores.put('S', 1);
        scores.put('T', 1);
        //TWO D, G = 2;
        scores.put('D', 2);
        scores.put('G', 2);
        //THREE B, C, M, P = 3;
        scores.put('B', 3);
        scores.put('C', 3);
        scores.put('M', 3);
        scores.put('P', 3);
        //FOUR F, H, V, W, Y = 4
        scores.put('F', 4);
        scores.put('H', 4);
        scores.put('V', 4);
        scores.put('W', 4);
        scores.put('Y', 4);
        //K = 5; J, X = 8; Q, Z = 10;
        scores.put('K', 5);
        scores.put('X', 8);
        scores.put('Q', 10);
        scores.put('Z', 10);

        //Using the hashmap to get the scores and add it to the total
        for (char c : string.toUpperCase().toCharArray()) {
            total += scores.get(c);
        }

        return total;
    }

    /**
     * DONE
     * WIP
     * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
     * <p>
     * The North American Numbering Plan (NANP) is a telephone numbering system used
     * by many countries in North America like the United States, Canada or Bermuda.
     * All NANP-countries share the same international country code: 1.
     * <p>
     * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
     * Area code, commonly known as area code, followed by a seven-digit local
     * number. The first three digits of the local number represent the exchange
     * code, followed by the unique four-digit number which is the subscriber
     * number.
     * <p>
     * The format is usually represented as
     * <p>
     * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
     * from 0 through 9.
     * <p>
     * Your task is to clean up differently formatted telephone numbers by removing
     * punctuation and the country code (1) if present.
     * <p>
     * For example, the inputs
     * <p>
     * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
     * the output
     * <p>
     * 6139950253
     * <p>
     * Note: As this exercise only deals with telephone numbers used in
     * NANP-countries, only 1 is considered a valid country code.
     */
    public String cleanPhoneNumber(String string) {
        // TODO Write an implementation for this method declaration

        StringBuilder phoneNumber = new StringBuilder();
        //add all numbers to the phoneNumber object
        for (char c : string.toCharArray()) {
            if (String.valueOf(c).matches("[0-9]")) {
                phoneNumber.append(c);
            }
        }
        //if first character is 1 then remove it
        if (phoneNumber.charAt(1) == '1') {
            phoneNumber.deleteCharAt(1);
        }
        //phone numbers can only be 10 digits long, if to long or short throw an exception
        if (phoneNumber.length() != 10) {
            throw new IllegalArgumentException("not a valid number size");
        }
        //verify that the nth place is not one NXX-NXX-XXXX
        int count = 0;
        for (char c : phoneNumber.toString().toCharArray()) {
            if (count == 3 || count == 0) {
                if (Integer.parseInt(String.valueOf(c)) == 1) {
                    throw new IllegalArgumentException("You can't have a 1 in that place");
                }
            }


        }
        return phoneNumber.toString();


      /*
      First attempt before redoing it the next day
      try {
            String temp = "";
            for (char c : string.toCharArray()) {
                if (Character.isDigit(c)) {
                    temp += c;
                }

            }
            if (temp.charAt(0) == '1') {
                temp.substring(1);
            }
            return temp;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /**
     * DONE
     * 6. Given a phrase, count the occurrences of each word in that phrase.
     * <p>
     * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
     * free: 1
     *
     * @param string
     * @return
     */
    public Map<String, Integer> wordCount(String string) {
        // TODO Write an implementation for this method declaration
        String[] split = string.split("[\\s,]+"); //split with regex
        HashMap<String, Integer> counter = new HashMap<>(); //hashmap seems to be the implied return value
        for (String word : split) {
            if (!counter.containsKey(word)) { //if it doesnt exist in the map then add it
                counter.put(word, 1);
            } else if (counter.containsKey(word)) {//if it exist, then updated it
                counter.put(word, (counter.get(word) + 1));
            }
        }
        return counter;
    }

    /** FAIL
     * 7. Implement a binary search algorithm.
     * <p>
     * Searching a sorted collection is a common task. A dictionary is a sorted list
     * of word definitions. Given a word, one can find its definition. A telephone
     * book is a sorted list of people's names, addresses, and telephone numbers.
     * Knowing someone's name allows one to quickly find their telephone number and
     * address.
     * <p>
     * If the list to be searched contains more than a few items (a dozen, say) a
     * binary search will require far fewer comparisons than a linear search, but it
     * imposes the requirement that the list be sorted.
     * <p>
     * In computer science, a binary search or half-interval search algorithm finds
     * the position of a specified input value (the search "key") within an array
     * sorted by key value.
     * <p>
     * In each step, the algorithm compares the search key value with the key value
     * of the middle element of the array.
     * <p>
     * If the keys match, then a matching element has been found and its index, or
     * position, is returned.
     * <p>
     * Otherwise, if the search key is less than the middle element's key, then the
     * algorithm repeats its action on the sub-array to the left of the middle
     * element or, if the search key is greater, on the sub-array to the right.
     * <p>
     * If the remaining array to be searched is empty, then the key cannot be found
     * in the array and a special "not found" indication is returned.
     * <p>
     * A binary search halves the number of items to check with each iteration, so
     * locating an item (or determining its absence) takes logarithmic time. A
     * binary search is a dichotomic divide and conquer search algorithm.
     */
    static class BinarySearch<T> {
        private List<T> sortedList;

        public int indexOf(T t) {
//        public <T extends Comparable<T>> int indexOf(T t) {
            // TODO Write an implementation for this method declaration
            int pointer = sortedList.size();
            for (int i = 0; i < sortedList.size(); i++) {
                if (sortedList.get(pointer / 2) == t) {
//                    System.out.println((sortedList.get(pointer / 2)));
//                    return i;
                } else {
//                    ( sortedList.get(pointer / 2)  > pointer )
//                            ? (pointer += pointer + pointer / 2)
//                            : (pointer -= pointer / 2);
                }
            }

            return 0;
        }

        public BinarySearch(List<T> sortedList) {
            super();
            this.sortedList = sortedList;
        }

        public List<T> getSortedList() {
            return sortedList;
        }

        public void setSortedList(List<T> sortedList) {
            this.sortedList = sortedList;
        }

    }

    /**
     * DONE
     * 8. Implement a program that translates from English to Pig Latin.
     * <p>
     * Pig Latin is a made-up children's language that's intended to be confusing.
     * It obeys a few simple rules (below), but when it's spoken quickly it's really
     * difficult for non-children (and non-native speakers) to understand.
     * <p>
     * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
     * the word. Rule 2: If a word begins with a consonant sound, move it to the end
     * of the word, and then add an "ay" sound to the end of the word. There are a
     * few more rules for edge cases, and there are regional variants too.
     * <p>
     * See http://en.wikipedia.org/wiki/Pig_latin for more details.
     *
     * @param string
     * @return
     */
    public String toPigLatin(String string) {
        // TODO Write an implementation for this method declaration
        String[] split = string.toLowerCase().split("[\\s,]+"); //split with regex
//        String[] sentence = new String[split.length];
        ArrayList<String> sentence = new ArrayList<>();
        for (String word : split) {
            sentence.add(vowelRecursive(word));
        }

        String temp = "";
        for (String w : sentence) {
            temp += w + " ";
        }

//        temp = temp.substring(0, 1).toUpperCase() + temp.substring(1);
        return temp.trim();
    }

    // additonal method to preserve DRY principle
    public static String vowelRecursive(String word) {
        if ("AEIOUaeiou".indexOf(word.charAt(0)) >= 0) {
            word += "ay";
            return word;
        } else {
            word = word.substring(1) + word.charAt(0);
            return vowelRecursive(word);
        }
    }


    /**
     * DONE
     * 9. An Armstrong number is a number that is the sum of its own digits each
     * raised to the power of the number of digits.
     * <p>
     * For example:
     * <p>
     * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
     * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
     * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
     * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
     * a number is an Armstrong number.
     *
     * @param input
     * @return
     */
    public boolean isArmstrongNumber(int input) {
        // TODO Write an implementation for this method declaration
        int armstrong = input;
        int temp = 0;
        char[] range = String.valueOf(input).toCharArray();
        for (char c : range) {
            temp += Math.pow(Integer.parseInt(String.valueOf(c)), range.length);
        }
        if (temp == armstrong) {
            return true;
        }

        return false;
    }

    /** WIP
     * 10. Compute the prime factors of a given natural number.
     * <p>
     * A prime number is only evenly divisible by itself and 1.
     * <p>
     * Note that 1 is not a prime number.
     *
     * @param l
     * @return
     */
    public List<Long> calculatePrimeFactorsOf(long l) {
        // TODO Write  an implementation for this method declaration

        List<Long> primeFactors = new ArrayList<>();


        while (l % 2 == 0) {
            primeFactors.add(l);
            l /= 2;
        }

        for (long el = 3; el <= Math.sqrt(l); el += 2) {
            while (l % el == 0) {
                primeFactors.add(el);
                l /= el;
            }
        }

        return primeFactors;
    }


    /**
     * DONE
     * 11. Create an implementation of the rotational cipher, also sometimes called
     * the Caesar cipher.
     * <p>
     * The Caesar cipher is a simple shift cipher that relies on transposing all the
     * letters in the alphabet using an integer key between 0 and 26. Using a key of
     * 0 or 26 will always yield the same output due to modular arithmetic. The
     * letter is shifted for as many values as the value of the key.
     * <p>
     * The general notation for rotational ciphers is ROT + <key>. The most commonly
     * used rotational cipher is ROT13.
     * <p>
     * A ROT13 on the Latin alphabet would be as follows:
     * <p>
     * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
     * stronger than the Atbash cipher because it has 27 possible keys, and 25
     * usable keys.
     * <p>
     * Ciphertext is written out in the same formatting as the input including
     * spaces and punctuation.
     * <p>
     * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
     * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
     * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
     * quick brown fox jumps over the lazy dog.
     */
    static class RotationalCipher {
        private int key;

        public RotationalCipher(int key) {
            super();
            this.key = key;
        }

        public String rotate(String string) {
            // TODO Write an implementation for this method declaration
            HashMap<Character, Character> alphabet = new HashMap<>();

            //Create a hashset of the alphabet and the rotational cypher
            IntStream.range((int) 'a', (int) 'z' + 1).forEach(e -> alphabet.put((char) e,
                    //this tenary operator is key to looping back to 'a' once the sum of the current letter and the key exceed the int value of 'z'
                    e + this.key > (int) 'z' ?
                            (char) (e - 26 + this.key % (int) 'z') :
                            (char) (e + this.key % (int) 'z')
            ));
            IntStream.range((int) 'A', (int) 'Z' + 1).forEach(e -> alphabet.put((char) e,
                    //this tenary operator is key to looping back to 'A' once the sum of the current letter and the key exceed the int value of 'Z'
                    e + this.key > (int) 'Z' ?
                            (char) (e - 26 + this.key % (int) 'Z') :
                            (char) (e + this.key % (int) 'Z')
            ));

            //create an array of chars to iterate and cypher with our created hashmap
            char[] original = string.toCharArray();
            //stringbuilder to store characters
            StringBuilder cyper = new StringBuilder();

            for (char c : original) {
                //if a letter then cyper
                if (String.valueOf(c).matches("[A-Za-z]")) {
                    cyper.append(alphabet.get(c));
                } else {
                    //otherwise add the unchanged character to the stringbuilder
                    cyper.append(c);
                }
            }


            return cyper.toString();
        }

    }

    /**
     * DONE
     * 12. Given a number n, determine what the nth prime is.
     * <p>
     * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
     * that the 6th prime is 13.
     * <p>
     * If your language provides methods in the standard library to deal with prime
     * numbers, pretend they don't exist and implement them yourself.
     *
     * @param i
     * @return
     */
    public int calculateNthPrime(int i) {
        //catch any negative or 0 valued input
        if (i <= 0) {
            throw new IllegalArgumentException("Illegal Argument");
        }
        // TODO Write an implementation for this method declaration
        //Chose a tree for absolute uniqueness and to sort it automatically
        //Any type of tree should work fine too
        TreeSet<Integer> primes = new TreeSet<>();
        primes.add(2);
        int pr = 2;
//While loop will run until the the size is equal to the desired nth prime
        while (primes.size() < i) {
            boolean marker = false;
            for (int j = 2; j < pr; j++) {
                //Composite numbers are caught here.
                if (pr % j == 0 && pr != j) {
                    marker = false;
                    break;
                }
                marker = true; //if not a composite number then its a prime
            }
            //use prime marker to add current pr(ime) value to the Primes TreeSet
            if (marker) {
                primes.add(pr);
            }
            pr++;
        }
        //return last value of the sorted TreeSet
        return primes.last();
    }

    /**
     * DONE
     * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
     * system created in the Middle East.
     * <p>
     * The Atbash cipher is a simple substitution cipher that relies on transposing
     * all the letters in the alphabet such that the resulting alphabet is
     * backwards. The first letter is replaced with the last letter, the second with
     * the second-last, and so on.
     * <p>
     * An Atbash cipher for the Latin alphabet would be as follows:
     * <p>
     * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
     * very weak cipher because it only has one possible key, and it is a simple
     * monoalphabetic substitution cipher. However, this may not have been an issue
     * in the cipher's time.
     * <p>
     * Ciphertext is written out in groups of fixed length, the traditional group
     * size being 5 letters, and punctuation is excluded. This is to make it harder
     * to guess things based on word boundaries.
     * <p>
     * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
     * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
     */
    static class AtbashCipher {

        public static char library(char c) {

            HashMap<Character, Character> alphabet = new HashMap<>();

            //Create a hashset of the alphabet and inverse by manipulating the int value of the given char with the help of streams
            IntStream.range((int) 'a', (int) 'z' + 1).forEach(e ->
                    //add the abc..
                    alphabet.put((char) e,
//                    and the int value of a + the inverse index of the current iteration
                            (char) ((int) 'a' + ((int) 'z' - e))
                    ));
            return alphabet.get(c);
        }

        /**
         * Question 13
         *
         * @param string
         * @return
         */


        public static String encode(String string) {
            // TODO Write an implementation for this method declaration
            ArrayList<Character> encode = new ArrayList<>();

            //for each loop goes thought the char array and if its a letter then refer to the library method which
            //gets the encoded value. If its a digit just add it regularly and ignore everything else
            for (char c : string.toLowerCase().toCharArray()) {
                if (String.valueOf(c).matches("[A-Za-z]")) {
                    encode.add(library(c));

                } else if (String.valueOf(c).matches("[0-9]")) {
                    encode.add(c);
                }
            }
            StringBuilder message = new StringBuilder();
            int count = 1;
            //the encode arraylist contains all the neccessary chars so now we need to seperate it at every 5th character
            for (char c : encode) {
                message.append(encode.get(count - 1));
                if (count % 5 == 0) {
                    message.append(" ");
                }
                count++;
            }

            return message.toString().trim();

        }

        /**
         * Question 14
         *
         * @param string
         * @return
         */

        //this follows the same precedures as encode except we dont have to seperate by space
        public static String decode(String string) {
            // TODO Write an implementation for this method declaration

            ArrayList<Character> encode = new ArrayList<>();

            //for each loop goes thought the char array and if its a letter then refer to the library method which
            //gets the dencoded value. If its a digit just add it regularly and ignore everything else
            for (char c : string.toLowerCase().toCharArray()) {
                if (String.valueOf(c).matches("[A-Za-z]")) {
                    encode.add(library(c));

                } else if (String.valueOf(c).matches("[0-9]")) {
                    encode.add(c);
                }
            }
            StringBuilder message = new StringBuilder();
            int count = 1;
            //the dencode arraylist contains all the neccessary chars so now we need to seperate it at every 5th character
            for (char c : encode) {
                message.append(encode.get(count - 1));
                count++;
            }

            return message.toString().trim();
        }
    }

    /**
     * Done
     * 15. The ISBN-10 verification process is used to validate book identification
     * numbers. These normally contain dashes and look like: 3-598-21508-8
     * <p>
     * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
     * a digit or an X only). In the case the check character is an X, this
     * represents the value '10'. These may be communicated with or without hyphens,
     * and can be checked for their validity by the following formula:
     * <p>
     * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
     * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
     * otherwise it is invalid.
     * <p>
     * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
     * and get:
     * <p>
     * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
     * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
     *
     * @param string
     * @return
     */
    public boolean isValidIsbn(String string) {
        // TODO Write an implementation for this method declaration
//        String regex = "[^X0-9.-_]";
        //Initial test to weed out any invalid
        ArrayList<Integer> isbn = new ArrayList<>();
        for (char c : string.toUpperCase().toCharArray()) {
            if (String.valueOf(c).matches("[0-9]+")) {
                isbn.add(Integer.parseInt(String.valueOf(c)));

            } else if (String.valueOf(c).matches("-")) {

            } else if (String.valueOf(c).matches("X")) {
                isbn.add(10);
            } else {
                return false;//illegal symbol or character
            }
        }
        //quick check (should be 10 numbers in total)
        if (isbn.size() != 10) return false;
        //counter
        AtomicInteger i = new AtomicInteger(11);
        //This stream will map each element with the product of the decreasing counter
        Integer sum = isbn.stream().map(e -> {
            i.getAndDecrement();
            return e * i.get();
        }).collect(Collectors.summingInt(Integer::intValue));//this terminal returns the sum of the altered arraylist

        //final check
        if (sum % 11 == 0) return true;

        //default return
        return false;
    }

    /**
     * DONE
     * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
     * gramma, "every letter") is a sentence using every letter of the alphabet at
     * least once. The best known English pangram is:
     * <p>
     * The quick brown fox jumps over the lazy dog.
     * <p>
     * The alphabet used consists of ASCII letters a to z, inclusive, and is case
     * insensitive. Input will not contain non-ASCII symbols.
     *
     * @param string
     * @return
     */
    public boolean isPangram(String string) {
        // TODO Write an implementation for this method declaration
        HashSet<Character> alphabet = new HashSet<>();

        //Create a has set of each lowercase letter in the alphabet
        IntStream.range((int) 'a', (int) 'z' + 1).forEach(
                e -> {
                    alphabet.add((char) e);
                }
        );
        //create an array of chars (lowercase)
        char[] phrase = string.toLowerCase().toCharArray();

        //loop through the char array and use the alphabet hashset as a checklist to check off if each character is found
        for (char l : phrase) {
            if (alphabet.contains(l)) {
                alphabet.remove(l);
            }
        }
        //if the phrase has all characters then the alphabet set should be empty
        if (alphabet.size() == 0) {
            return true;
        }
        return false;
    }

    /** FAIL
     * 17. Calculate the moment when someone has lived for 10^9 seconds.
     * <p>
     * A gigasecond is 109 (1,000,000,000) seconds.
     *
     * @param given
     * @return
     */
    public Temporal getGigasecondDate(Temporal given) {
        // TODO Write an implementation for this method declaration
        long giga = 1000000000;
        LocalDateTime later = (LocalDateTime) given.plus(giga, ChronoUnit.SECONDS);
        return later;
    }

    /**
     * DONE
     * 18. Given a number, find the sum of all the unique multiples of particular
     * numbers up to but not including that number.
     * <p>
     * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
     * get 3, 5, 6, 9, 10, 12, 15, and 18.
     * <p>
     * The sum of these multiples is 78.
     *
     * @param i
     * @param set
     * @return
     */
    public int getSumOfMultiples(int i, int[] set) {
        // TODO Write an implementation for this method declaration
        //HashSet to store numbers
        HashSet<Integer> intSet = new HashSet<>();

        //Iterate with the given int
        IntStream.range(1, i).forEach(e -> {
//            for each number in range check if each element are multiple of the given int
            for (int num : set) {
                if ((Integer) e % num == 0) {
                    intSet.add((Integer) e);
                }
            }
        });

        //Use iterator to interate the created hashset
        Iterator sum = intSet.iterator();
        int answer = 0;
        //generate sum
        while (sum.hasNext()) {
            answer += (Integer) sum.next();
        }

        return answer;
    }

    /**
     * DONE
     * 19. Given a number determine whether or not it is valid per the Luhn formula.
     * <p>
     * The Luhn algorithm is a simple checksum formula used to validate a variety of
     * identification numbers, such as credit card numbers and Canadian Social
     * Insurance Numbers.
     * <p>
     * The task is to check if a given string is valid.
     * <p>
     * Validating a Number Strings of length 1 or less are not valid. Spaces are
     * allowed in the input, but they should be stripped before checking. All other
     * non-digit characters are disallowed.
     * <p>
     * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
     * the Luhn algorithm is to double every second digit, starting from the right.
     * We will be doubling
     * <p>
     * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
     * then subtract 9 from the product. The results of our doubling:
     * <p>
     * 8569 2478 0383 3437 Then sum all of the digits:
     * <p>
     * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
     * then the number is valid. This number is valid!
     * <p>
     * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
     * digits, starting from the right
     * <p>
     * 7253 2262 5312 0539 Sum the digits
     * <p>
     * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
     * this number is not valid.
     *
     * @param string
     * @return
     */
    public boolean isLuhnValid(String string) {
        // initial catch clause to catch any obvious invalid entries
        try {
            Integer.valueOf(string.replaceAll("\\s", ""));
        } catch (NumberFormatException e) {
            return false;
        }
        char[] primal = string.replaceAll("\\s", "").toCharArray();
        int count = 1;
        int sum = 0;
        for (char c : primal) {
            int num = Integer.parseInt(String.valueOf(c));
            if (count % 2 == 0) {
                if ((num * 2) > 9) {
                    sum += (num * 2) - 9;
                } else {
                    sum += num * 2;
                }
            } else {
                sum += num;
            }
            count++;
        }

//        ArrayList<Character> numbers = new ArrayList<Character>(primal);

       /*
       A failed attempt to use streams... will work on it more on my own time
       List numbers = Arrays.asList(primal);
        AtomicInteger count = new AtomicInteger(0);
        numbers.stream().map(num ->{
            count.getAndIncrement();
            if (count.get() % 2 == 0) {
                if (((Integer) num * 2) > 9) {
                    return  (Integer) num * 2 - 9;
                } else {
                    return (Integer) num * 2;
                }
            } else {

                return (Integer) num;
            }

        });

        System.out.println(numbers);
        numbers.forEach(e -> System.out.println(e));*/


        if (sum % 10 == 0) {


            return true;
        }
        return false;
    }

    /**
     * DONE
     * 20. Parse and evaluate simple math word problems returning the answer as an
     * integer.
     * <p>
     * Add two numbers together.
     * <p>
     * What is 5 plus 13?
     * <p>
     * 18
     * <p>
     * Now, perform the other three operations.
     * <p>
     * What is 7 minus 5?
     * <p>
     * 2
     * <p>
     * What is 6 multiplied by 4?
     * <p>
     * 24
     * <p>
     * What is 25 divided by 5?
     * <p>
     * 5
     *
     * @param string
     * @return
     */
    public int solveWordProblem(String string) {
        // TODO Write an implementation for this method declaration
        //convert to a String array making sure to remove th question mark in the end
        String[] baseStringArr = string.substring(0, string.length() - 1).split(" ");
        //Arraylist to hold number
        ArrayList<Integer> equation = new ArrayList<>();
        //iterate to find numbers
        for (String word : baseStringArr) {
            try {
                if (Integer.valueOf(word) instanceof Integer) {
                    equation.add(Integer.valueOf(word));
                }
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        }
        //iterate a second time to calculate value
        for (String word : baseStringArr) {
            switch (word) {
                case "plus":
                    System.out.println(equation.toString());
                    return equation.get(0) + equation.get(1);
                case "minus":
                    return equation.get(0) - equation.get(1);
                case "multiplied":
                    return equation.get(0) * equation.get(1);
                case "divided":
                    return equation.get(0) / equation.get(1);
                default:

            }
        }
        //default return
        return 0;
    }


}
